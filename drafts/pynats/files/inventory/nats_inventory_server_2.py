# -*- coding: utf-8 -*-
import asyncio
from nats.aio.client import Client as NATS

import csv
import json

from .helper import access_response, response, error_response, parameters
import logging

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-n", "--nats", help="nats url server nats://127.0.0.1:4222")

args = parser.parse_args()
if args.nats:
    NATS_SERVER = args.nats
else:
    NATS_SERVER = "nats://127.0.0.1:4222"

# création de l'objet logger qui va nous servir à écrire dans les logs
logger = logging.getLogger()
# on met le niveau du logger à DEBUG, comme ça il écrit tout
logger.setLevel(logging.DEBUG)

# création d'un formateur qui va ajouter le temps, le niveau
# de chaque message quand on écrira un message dans le log
formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(filename)s  :: %(message)s')
# création d'un handler qui va rediriger une écriture du log vers
# un fichier en mode 'append', avec 1 backup et une taille max de 1Mo
# file_handler = RotatingFileHandler('activity.log', 'a', 1000000, 1)
# on lui met le niveau sur DEBUG, on lui dit qu'il doit utiliser le formateur
# créé précédement et on ajoute ce handler au logger
# file_handler.setLevel(logging.DEBUG)
# file_handler.setFormatter(formatter)
# logger.addHandler(file_handler)

# création d'un second handler qui va rediriger chaque écriture de log
# sur la console
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
stream_handler.setLevel(logging.DEBUG)
logger.addHandler(stream_handler)

""" 
charge le fichier PF-RelationVmboxVersBoxv2.csv en mémoire sous forme de liste de dictionnaires
chaque dictionnaire correspond à un device (une box)
Puis attend une request contenant un json avec fti, ou un nom de box ou une adresse MAC 
Detecte automatiquement, si la recherche se fera sur un fti, un nom de box ou une adresse MAC
Publie sur le bus NATS le résultat  
Quand la requete provient d'une resgate une requete avec le sujet access.inventory.livebox est reçu on doit lui répondre 
avec les droits que l'on donne à la requete et notament les droits sur le find
Source https://github.com/nats-io/nats.py
"""

#dates = {"day": "23", "month": "1", "year": "2020", "hour": "10", "minute": "15", "seconde": "54", "tzone": "utc"}
#"""YYYY-MM-DD[*HH[:MM[:SS[.fff[fff]]]][+HH:MM[:SS[.ffffff]]]]"""
dates = "2020-01-23-15:12:30.5000"
# fieldnames=['name','domain','room','LB_BENCHE','LB_IP','LB_TYPE','LB_PASS','LB_WIFI_KEY','PDU_NAME','PDU_IP','PDU_SOCKET','NODE','PHONE','PHONE_PORT','FTI','MAC',"client","project","start_date","end_date"]
fieldnames = ['name', 'lb_type', 'serial_number', 'mac', 'lb_wifi_key', 'lb_pass', 'lb_ip', 'lan', 'ex', 'equipment',
              'equipment_type', 'equipment_brand', 'fti', 'remote_id', 'far_id', 'aid', 'lb_benche', 'pdu_name',
              'pdu_ip', 'pdu_socket', 'phone', 'phone_port', 'distant_phone', 'distant_phone_port', 'stb_type',
              'stb_mac', 'client', 'project', 'start_date', 'end_date']
csv_name = 'PF-RelationVmboxVersBoxv2.csv'

device_list = []


def csv_load():
    """

    :return:
    """
    device_list[:] = []
    with open(csv_name, newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        # Ajoute les lignes lu dans le fichier dans une liste de dictionnaire
        for row in reader:
            device_list.append(row)
    # js contient les données du fichier sous la forme d'un json
    js = json.dumps(device_list, indent=4)
    # logger.debug(js)
    # On charge le fichier CSV d'inventaire


#csv_load()


def find_a_box(dico):
    """
    Recherche une box dans le fichier chargé en mémoire
    si trouvé renvoi un dictionnaire pour la box sinon un dictionnaire error
    """
    print("----------------------------------------------------------------------------------------")
    cle = ''
    for cle in dico.keys():
        logger.debug("la recherche va se faire sur la cle : {i} : {critere}".format(i=cle, critere=dico[cle]))
    valeur = dico[cle]

    trouve = False
    lesbox = []
    for box in device_list:
        labox = dict(box)
        logger.debug("labox={labox} {box}".format(labox=labox, box=dict(box)))
        if labox[cle] == valeur:
            # Converti le dictionnaire en json formaté proprement
            lesbox.append(labox)
            trouve = True
            break
    # print("----------------------------------------------------------------------------------------")
    # print(json.dumps(lesbox,indent=4))
    # print("----------------------------------------------------------------------------------------")
    if trouve:
        r = response(lesbox)
    else:
        # r = {"error": {"code" :"404", "message" : "DEVICE NOT FOUND"}}
        # print("le device correspondant à la donnee {cle}:{device} a generer l'erreur suivante : {jzone}".format(cle=cle,device=device,jzone=json.dumps(r,indent=4)))
        r = error_response("404", "DEVICE NOT FOUND")
    return r


def find_multi_box(dico):
    """
    Recherche de box multiple dans le fichier chargé en mémoire
    si trouvé renvoi un dictionnaire ou une liste de box sinon un dictionnaire error
    """
    print("----------------------------------------------------------------------------------------")
    cle = ''
    nbkey = 1
    keys = []
    logger.debug("Search keys of the query")
    for cle in dico.keys():
        keys.append(cle)
        logger.debug("key {nbkey} is {i} : {critere}".format(nbkey=nbkey, i=cle, critere=dico[cle]))
    # cle=keys[1]
    # print("cle=" + cle)
    valeur = dico[cle]
    trouve = False
    lesbox = []
    # labox = {}
    for box in device_list:
        labox = dict(box)
        # print("labox :{labox}".format(labox=labox))
        if labox[cle] == valeur:
            logger.debug("labox={labox} {box}".format(labox=labox, box=dict(box)))
            # logger.debug("labox={labox}".format(labox=labox))
            # Converti le dictionnaire en json formaté proprement
            lesbox.append(labox)
            # print(json.dumps(labox,indent=4))
            # print("le device correspondant à la donnee " + device + " recue par message est " + labox['name'])
            # print("----------------------------------------------------------------------------------------")
            trouve = True
    # print("----------------------------------------------------------------------------------------")
    # print(json.dumps(lesbox,indent=4))
    # print("----------------------------------------------------------------------------------------")
    if trouve:
        r = response(lesbox)
    #   print(r)
    else:
        r = error_response("404", "DEVICE NOT FOUND")
    # print("le ou les device(s) correspondant à la donnee " + device + " a generer l'erreur suivante " + json.dumps(r,indent=4))
    return r


async def run(loop):
    nc = NATS()
    logger.info("Connexion au bus NATS : {}".format(NATS_SERVER))
    await nc.connect(NATS_SERVER, ping_interval=30, loop=loop)

    async def inventory_find_request(msg):
        """
        handler declenché sur la reception d'un message find
        recoit en entree un json de la forme {"cle" : "valeur"}
        si le device est trouve renvoi un json avec tous les champs du device
        sinon renvoi un json d'erreur
        """
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        rq = json.loads(data)
        logger.info("Reception d'un message find")
        content = find_a_box(parameters(msg))
        # content = find_a_box(rq['params'])
        logger.info("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply, json.dumps(content).encode())

    async def inventory_query_request(msg):
        """
        handler declenché sur la reception d'un message query
        recoit en entree un json de la forme {"cle" : "valeur"}
        si le ou les device est/sont trouve(s) renvoi un json avec tous les champs du/des device(s)
        sinon renvoi un json d'erreur
        """
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        rq = json.loads(data)
        logger.info("Reception d'un message query")
        content = find_multi_box(parameters(msg))
        # content = find_a_box(rq['params'])
        logger.info("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply, json.dumps(content).encode())

    async def inventory_access_request(msg):
        """
        handler declenché sur la reception d'un message access en provenance de la resgate
        recoit en entree un json
        renvoi les droits dans un json
        """
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        # rq = json.loads(data)
        content = '{"result" : {"get": true, "call":"set,find,query,ping"}}'
        logger.info("Reception d'un message access")
        # C'est ici que l'on construit la reponse a la requete access que nous envoie la resgate
        # Donc chaque fois que l'on crée une requête utilisable via la resgate il faut y ajouter
        # son motclef ici ex:  content = json.dumps(access_response("find,query,reserver,liberer,delivrer,fairepousser"))
        content = json.dumps(access_response("set,ping,help,,find,query,reread"))
        logger.info("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply, content.encode())

    async def inventory_ping_request(msg):
        """
        handler declenché sur la reception d'un message access en provenance de la resgate
        recoit en entree un json
        renvoi les droits dans un json
        """
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        logger.info("Reception d'un message access")
        # C'est ici que l'on construit la reponse a la requete pong que nous envoie la resgate
        # Donc chaque fois que l'on crée une requête utilisable via la resgate il faut y ajouter
        content = json.dumps(access_response("pong"))
        logger.info("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply, content.encode())

    async def inventory_reread_request(msg):
        """
        handler declenché sur la reception d'un message inventory_reread_request en provenance de la resgate
        recoit en entree un json
        renvoi un message 200
        """
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        logger.info("Reception d'un message reread")
        # C'est ici que l'on construit la reponse a la requete access que nous envoie la resgate
        # Donc chaque fois que l'on crée une requête utilisable via la resgate il faut y ajouter
        # son motclef ici ex:  content = json.dumps(access_response("find,query,reserver,liberer,delivrer,fairepousser"))
        # content = json.dumps(response({result:{"reponse" : "OK"}}))
        logger.info("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        csv_load()
        # await nc.publish(reply,content.encode())

    # Use queue named 'workers' for distributing requests
    # among subscribers.

    # On met un handler sur la reception d'un message access.inventory.livebox
    logger.info("Souscription au sujet access.inventory.livebox")
    sid1 = await nc.subscribe("access.inventory.livebox", "workers", cb=inventory_access_request)

    logger.info("Souscription au sujet call.inventory.livebox.find")
    # On met un handler sur la reception d'un message call.inventory.livebox.find
    sid2 = await nc.subscribe("call.inventory.livebox.find", "workers", cb=inventory_find_request)

    logger.info("Souscription au sujet call.inventory.livebox.query")
    sid3 = await nc.subscribe("call.inventory.livebox.query", "workers", cb=inventory_query_request)

    logger.info("Souscription au sujet call.inventory.livebox.reread")
    sid4 = await nc.subscribe("call.inventory.livebox.reread", "workers", cb=inventory_reread_request)

    logger.info("Souscription au sujet call.inventory.livebox.ping")
    sid5 = await nc.subscribe("call.inventory.livebox.ping", "workers", cb=inventory_ping_request)

    logger.info("Souscription au sujet call.inventory.livebox.help")
    sid5 = await nc.subscribe("call.inventory.livebox.help", "workers", cb=inventory_access_request)
    # On boucle a l'infini
    while True:  # nbm <= nbmessages:
        await nc.flush(1)

    # Remove interest in subscription.
    # print("unsubscribe all")
    # await nc.unsubscribe(sid1)
    # await nc.unsubscribe(sid2)
    # await nc.unsubscribe(sid3)
    # await nc.unsubscribe(sid4)
    # # Terminate connection to NATS.
    # print("close NATS connection")
    # await nc.close()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))
    loop.close()
