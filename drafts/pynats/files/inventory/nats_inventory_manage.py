# -*- coding: utf-8 -*-
import asyncio
from nats.aio.client import Client as NATS
from nats.aio.errors import ErrConnectionClosed, ErrTimeout, ErrNoServers
import csv
import json

from .helper import access_response, response, error_response, parameters

import logging
from logging.handlers import RotatingFileHandler
import argparse



parser = argparse.ArgumentParser()
parser.add_argument("-n","--nats", help="nats url server nats://127.0.0.1:4222")

args = parser.parse_args()
if args.nats:
    NATS_SERVER=args.nats
else:
    NATS_SERVER="nats://127.0.0.1:4222"


# création de l'objet logger qui va nous servir à écrire dans les logs
logger = logging.getLogger()
# on met le niveau du logger à DEBUG, comme ça il écrit tout
logger.setLevel(logging.DEBUG)

# création d'un formateur qui va ajouter le temps, le niveau
# de chaque message quand on écrira un message dans le log
formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(filename)s  :: %(message)s')
# création d'un handler qui va rediriger une écriture du log vers
# un fichier en mode 'append', avec 1 backup et une taille max de 1Mo
#file_handler = RotatingFileHandler('activity.log', 'a', 1000000, 1)
# on lui met le niveau sur DEBUG, on lui dit qu'il doit utiliser le formateur
# créé précédement et on ajoute ce handler au logger
#file_handler.setLevel(logging.DEBUG)
#file_handler.setFormatter(formatter)
#logger.addHandler(file_handler)

# création d'un second handler qui va rediriger chaque écriture de log
# sur la console
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
stream_handler.setLevel(logging.DEBUG)
logger.addHandler(stream_handler)





""" 
charge le fichier PF-RelationVmboxVersBoxv2.csv en mémoire sous forme de liste de dictionnaires
chaque dictionnaire correspond à un device (une box)
Puis attend une request contenant un json avec fti, ou un nom de box ou une adresse MAC 
Detecte automatiquement, si la recherche se fera sur un fti, un nom de box ou une adresse MAC
Publie sur le bus NATS le résultat  
Quand la requete provient d'une resgate une requete avec le sujet access.manage_inventory.livebox est reçu on doit lui répondre 
avec les droits que l'on donne à la requete et notament les droits sur le find
Source https://github.com/nats-io/nats.py
"""


#fieldnames=['name','domain','room','LB_BENCHE','LB_IP','LB_TYPE','LB_PASS','LB_WIFI_KEY','PDU_NAME','PDU_IP','PDU_SOCKET','NODE','PHONE','PHONE_PORT','FTI','MAC',"client","project","start_date","end_date"]
fieldnames=['name','lb_type','serial_number','mac','lb_wifi_key','lb_pass','lb_ip','lan','ex','equipment','equipment_type','equipment_brand','fti','remote_id','far_id','aid','lb_benche','pdu_name','pdu_ip','pdu_socket','phone','phone_port','distant_phone','distant_phone_port','stb_type','stb_mac','client','project','start_date','end_date']
csv_name='PF-RelationVmboxVersBoxv2.csv'

device_list = []
def csv_load():
    device_list[:] = []
    with open(csv_name, newline='') as csvfile:
        reader = csv.DictReader(csvfile,delimiter=';')
        #Ajoute les lignes lu dans le fichier dans une liste de dictionnaire
        for row in reader:
            device_list.append(row)
    #js contient les données du fichier sous la forme d'un json
    js = json.dumps( device_list, indent=4)

#On charge le fichier CSV d'inventaire
#csv_load()


def csv_write(lesbox):
    """ 
    Ecrit le fichier
    """
    try:
        #Ouverture du fichier
        with open(csv_name,"w") as csvfile:
            #declaration du DictWriter
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames,delimiter=';')
            #declaration du DictWriter
            #writer = csv.writer(csvfile)
            writer.writeheader()
            logger.debug("lesbox {row}".format(row=lesbox))
            for row in lesbox:
                # print("ecriture de {row}".format(row=row))
                logger.debug("ecriture de {row}".format(row=row))
                writer.writerow(row)
            #csvfile.close()
            #csv_load()
    except csv.Error as e:
        print("ERREUR")

def add_a_box(a_box):
    '''
    Recherche une box dans le fichier chargé en mémoire
    si trouvé renvoi un dictionnaire pour la box sinon un dictionnaire error
    '''
    print("----------------------------------------------------------------------------------------")
    cle = ''
    labox = dict(a_box)
    device_list.append(labox)
    csv_write(device_list)
    try :
       r = response( labox )
    except :
       #r = {"error": {"code" :"404", "message" : "DEVICE NOT FOUND"}}
       #print("le device correspondant à la donnee {cle}:{device} a generer l'erreur suivante : {jzone}".format(cle=cle,device=device,jzone=json.dumps(r,indent=4)))
       r = error_response("405","ERROR ADDING A BOX")
    return r
    
def del_a_box(a_box):
    '''
    Recherche une box dans le fichier chargé en mémoire
    si trouvé renvoi un dictionnaire pour la box sinon un dictionnaire error
    '''
    print("----------------------------------------------------------------------------------------")
    cle = ''
    labox = a_box
    lesbox = json.dumps(device_list)
    losboxos = []
    for box in device_list:
        if box['name'] != labox['name']:
            print("if {box} != {labox}".format(box=box['name'],labox=labox['name']))
            losboxos.append(box)
    #device_list = copy(lesbox)
    #print(losboxos)
    try :
#        for box in device_list:
#            print(box)
#            break
        csv_write(losboxos)
        r = response( labox )
    except :
       #r = {"error": {"code" :"404", "message" : "DEVICE NOT FOUND"}}
       #print("le device correspondant à la donnee {cle}:{device} a generer l'erreur suivante : {jzone}".format(cle=cle,device=device,jzone=json.dumps(r,indent=4)))
       r = error_response("406","ERROR DELETING A BOX")
    return r

def book_a_box(a_box):
    '''
    Recherche une box dans le fichier chargé en mémoire
    si trouvé renvoi un dictionnaire pour la box sinon un dictionnaire error
    '''
    print("----------------------------------------------------------------------------------------")
    cle = ''
    labox = a_box
    lesbox = json.dumps(device_list)
    losboxos = []
    box_retour = []

    for box in device_list:
        if box['name'] == labox['name']:
            box_retour.append(box)
            box["client"] = labox["client"]
            box["project"] = labox["project"]
            box["start_date"] = labox["start_date"]
            box["end_date"] = labox["end_date"]
            #print(box)
        losboxos.append(box)
            #losboxos.append(box)
    #device_list = copy(lesbox)
    #print(losboxos)
    try :
        #Reecriture du fichier des box
        csv_write(losboxos)
        r = response( box_retour)
    except :
       #r = {"error": {"code" :"404", "message" : "DEVICE NOT FOUND"}}
       #print("le device correspondant à la donnee {cle}:{device} a generer l'erreur suivante : {jzone}".format(cle=cle,device=device,jzone=json.dumps(r,indent=4)))
       r = error_response("407","ERROR BOOKING A BOX")
    return r

def release_a_box(a_box):
    '''
    Recherche une box dans le fichier chargé en mémoire
    si trouvé renvoi un dictionnaire pour la box sinon un dictionnaire error
    '''
    print("----------------------------------------------------------------------------------------")
    cle = ''
    labox = a_box
    lesbox = json.dumps(device_list)
    losboxos = []
    box_retour = []
    for box in device_list:
        if box['name'] == labox['name']:
            box_retour.append(box)
            box["client"] = ""
            box["project"] = ""
            box["start_date"] = ""
            box["end_date"] = ""
            #print(box)
        losboxos.append(box)
            #losboxos.append(box)
    #device_list = copy(lesbox)
    #print(losboxos)
    try :
        #Reecriture du fichier des box
        csv_write(losboxos)
        r = response(box_retour)
    except :
        r = error_response("408","ERROR RELEASING A BOX")
    return r

async def run(loop):
    nc = NATS()
    logger.info("Connexion au bus NATS : {}".format(NATS_SERVER))
    await nc.connect(NATS_SERVER, ping_interval=30,loop=loop)


    async def manage_inventory_add_a_box_request(msg):
        '''
        handler declenché sur la reception d'un message add_a_box
        recoit en entree un json de la forme {"cle" : "valeur"}
        si le ou les device est/sont trouve(s) renvoi un json avec tous les champs du/des device(s)
        sinon renvoi un json d'erreur
        '''
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        rq = json.loads(data)
        print("Reception d'un message add_a_box")
        print("Ajout de la box {labox}".format(labox=data))
        content = add_a_box(parameters(msg)) 
        print("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply,json.dumps(content).encode()) 
        await nc.publish("call.manage_inventory.livebox.reread","A relire".encode()) 
        await nc.publish("call.inventory.livebox.reread","A relire".encode()) 

    async def manage_inventory_del_a_box_request(msg):
        '''
        handler declenché sur la reception d'un message del_a_box
        recoit en entree un json de la forme {"cle" : "valeur"}
        si le ou les device est/sont trouve(s) renvoi un json avec tous les champs du/des device(s)
        sinon renvoi un json d'erreur
        '''
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        rq = json.loads(data)
        logger.info("Reception d'un message del_a_box")
        content = del_a_box(parameters(msg))
        logger.info("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply,json.dumps(content).encode()) 
        await nc.publish("call.manage_inventory.livebox.reread","A relire".encode()) 
        await nc.publish("call.inventory.livebox.reread","A relire".encode()) 

    async def manage_inventory_book_a_box_request(msg):
        '''
        handler declenché sur la reception d'un message book
        recoit en entree un json de la forme {"cle" : "valeur"}
        si le ou les device est/sont trouve(s) renvoi un json avec tous les champs du/des device(s)
        sinon renvoi un json d'erreur
        '''
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        rq = json.loads(data)
        logger.info("Reception d'un message book")
        content = book_a_box(parameters(msg))
        logger.info("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply,json.dumps(content).encode())
        await nc.publish("call.manage_inventory.livebox.reread","A relire".encode())
        await nc.publish("call.inventory.livebox.reread","A relire".encode())

    async def manage_inventory_release_a_box_request(msg):
        '''
        handler declenché sur la reception d'un message book
        recoit en entree un json de la forme {"cle" : "valeur"}
        si le ou les device est/sont trouve(s) renvoi un json avec tous les champs du/des device(s)
        sinon renvoi un json d'erreur
        '''
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        rq = json.loads(data)
        print("Reception d'un message release")
        content = release_a_box(parameters(msg))
        logger.info("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        #print("content=<<<<{content}>>>>".format(content=content))
        await nc.publish(reply,json.dumps(content).encode())
        await nc.publish("call.manage_inventory.livebox.reread","A relire".encode())
        await nc.publish("call.inventory.livebox.reread","A relire".encode())

    async def manage_inventory_ping_request(msg):
        '''
        handler declenché sur la reception d'un message access en provenance de la resgate
        recoit en entree un json
        renvoi les droits dans un json
        '''
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        logger.info("Reception d'un message ping")
        #C'est ici que l'on construit la reponse a la requete pong que nous envoie la resgate
        #Donc chaque fois que l'on crée une requête utilisable via la resgate il faut y ajouter
        content = json.dumps(access_response("pong"))
        print("Received a message on '{subject} {reply}': {data}".format(
                        subject=subject, reply=reply, data=data))
        await nc.publish(reply, content.encode())

    async def manage_inventory_reread_request(msg):
        '''
        handler declenché sur la reception d'un message manage_inventory_reread_request en provenance de la resgate
        recoit en entree un json
        renvoi un message 200
        '''
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        logger.info("Reception d'un message reread")
        #C'est ici que l'on construit la reponse a la requete access que nous envoie la resgate
        #Donc chaque fois que l'on crée une requête utilisable via la resgate il faut y ajouter
        #son motclef ici ex:  content = json.dumps(access_response("find,query,reserver,liberer,delivrer,fairepousser"))
        #content = json.dumps(response({result:{"reponse" : "OK"}}))
        logger.info("Received a message on '{subject} {reply}': {data}".format(
                        subject=subject, reply=reply, data=data))
        csv_load()
        #await nc.publish(reply,content.encode())


    async def manage_inventory_access_request(msg):
        '''
        handler declenché sur la reception d'un message access en provenance de la resgate
        recoit en entree un json 
        renvoi les droits dans un json
        '''
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        #rq = json.loads(data)
        #content = '{"result" : {"get": true, "call":"set,find,query"}}'
        logger.info("Reception d'un message access")
        #C'est ici que l'on construit la reponse a la requete access que nous envoie la resgate
        #Donc chaque fois que l'on crée une requête utilisable via la resgate il faut y ajouter
        #son motclef ici ex:  content = json.dumps(access_response("find,query,reserver,liberer,delivrer,fairepousser"))
        content = json.dumps(access_response("set,ping,help,find,query,add_a_box,del_a_box,book,release,update,reread"))
        print(content)
        logger.info("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply,content.encode()) 

    # Use queue named 'workers' for distributing requests
    # among subscribers.
    # On met un handler sur la reception d'un message access.manage_inventory.livebox
    logger.info("Souscription au sujet help.manage_inventory.livebox")
    sid0 = await nc.subscribe("call.manage_inventory.livebox.help", "workers", cb=manage_inventory_access_request)
    # On met un handler sur la reception d'un message access.manage_inventory.livebox
    logger.info("Souscription au sujet access.manage_inventory.livebox")
    sid1 = await nc.subscribe("access.manage_inventory.livebox", "workers", cb=manage_inventory_access_request)

    logger.info("Souscription au sujet call.manage_inventory.livebox.add_a_box")
    # On met un handler sur la reception d'un message call.manage_inventory.livebox.add_a_box
    sid2 = await nc.subscribe("call.manage_inventory.livebox.add_a_box", "workers", cb=manage_inventory_add_a_box_request)


    # On met un handler sur la reception d'un message access.manage_inventory.livebox.reread
    logger.info("Souscription au sujet call.manage_inventory.livebox.reread")
    sid3 = await nc.subscribe("call.manage_inventory.livebox.reread", "workers", cb=manage_inventory_reread_request)

    logger.info("Souscription au sujet call.manage_inventory.livebox.ping")
    sid4 = await nc.subscribe("call.manage_inventory.livebox.ping", "workers", cb=manage_inventory_ping_request)

    logger.info("Souscription au sujet call.manage_inventory.livebox.del_a_box")
    sid5 = await nc.subscribe("call.manage_inventory.livebox.del_a_box", "workers", cb=manage_inventory_del_a_box_request)

    logger.info("Souscription au sujet call.manage_inventory.livebox.book")
    sid6 = await nc.subscribe("call.manage_inventory.livebox.book", "workers", cb=manage_inventory_book_a_box_request)

    logger.info("Souscription au sujet call.manage_inventory.livebox.release")
    sid7 = await nc.subscribe("call.manage_inventory.livebox.release", "workers", cb=manage_inventory_release_a_box_request)

    #On boucle a l'infini
    while True: #nbm <= nbmessages:
        await nc.flush(1)

    # Remove interest in subscription.
    print("unsubscribe all")
    await nc.unsubscribe(sid0)
    await nc.unsubscribe(sid1)
    await nc.unsubscribe(sid2)
    await nc.unsubscribe(sid3)
    await nc.unsubscribe(sid4)
    await nc.unsubscribe(sid5)
    await nc.unsubscribe(sid6)
    await nc.unsubscribe(sid7)
#    await nc.unsubscribe(sid3)
    # Terminate connection to NATS.
    print("close NATS connection")
    await nc.close()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))
    loop.close()
