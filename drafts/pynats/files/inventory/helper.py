# -*- coding: utf-8 -*-
import json


def access_response(methodes):
    """
    """
    # print("access_response()")
    # #smethodes = ",".join(*methodes)
    content = {"result": {"get": True}}
    content["result"]["call"] = methodes
    # print(content)
    # content={"result" : {"get": True, "call":"set,find"}}
    return content


def response(data):
    """
    """
    return {"result": data}


def error_response(code, message):
    """
    """
    return {"error": {"code": code, "message": message}}


def parameters(msg):
    """
    le parametre msg est un message de type NATS.msg
    """
    #    a = {}
    #    a = json.loads(msg.data.decode())
    #    print(a['params'] )
    #    b=a['params']
    #    #return b
    return json.loads(msg.data.decode())['params']
