# build the docker image
docker build --force-rm --no-cache -t inventory --network host .

# archive image
docker save inventory:latest | gzip > inventory_latest.tgz

