#include .env

PROJECTNAME=$(shell basename "$(PWD)")

GOBASE=$(shell pwd)
#GOPATH=$(GOBASE)


# PID file will store the server process id when it's running on development mode
NATS_PID=/tmp/.$(PROJECTNAME)-nats-server.pid

# Make is verbose in Linux. Make it silent.
#MAKEFLAGS += --silent

hello :
	echo "Hello ${PROJECTNAME} at ${GOBASE}, GOPATH=${GOPATH}";


start-nats:
	@echo "start nats-server"
	nats-server 2>&1 & echo $$! > $(NATS_PID)
	@cat $(NATS_PID) | sed "/^/s/^/  \>  PID: /"

stop-nats:
	@-touch $(NATS_PID)
	@-kill `cat $(NATS_PID)` 2> /dev/null || true
	@-rm $(NATS_PID)


init:
	mkdir -p build/images
	mkdir -p build/bin
	mkdir -p build/etc
	docker pull alpine

#
# fetch products ( consul, nomad, nats-server, resgate )
#
fetch-products:
	@echo "fetch products ( consul, nomad, nats-server, resgate) "
	cd builder/products && ./fetch_linux.sh
	mv builder/products/consul build/bin/
	mv builder/products/nomad build/bin/
	mv builder/products/nats-server build/bin/
	mv builder/products/resgate build/bin/



#
# build base images
#
build-base-all: build-base-godev build-base-gopacket

build-base-godev:
	@echo "build docker image godev:latest"
	cd base/godev && ./build.sh

build-base-gopacket:
	@echo "build docker image gopacket:latest"
	cd base/gopacket && ./build.sh

#
# build services images
#
build-services: build-base-archive build-lbservices build-snifferlog build-snifferne

build-base-archive:
	@echo "build [bases]_latest.tgz"
	cd builder/bases  && ./build.sh
	cp builder/bases/*.tgz ./build/images
	rm builder/bases/*.tgz


build-lbservices:
	@echo "build lbservices_latest.tgz"
	cd builder/lbservices && rm -f lbservices_latest.tgz && ./build.sh
	mv builder/lbservices/lbservices_latest.tgz ./build/images/


build-snifferlog:
	@echo "build snifferlog_latest.tgz"
	cd builder/rtlog && rm -f snifferlog_latest.tgz  && ./build.sh
	mv builder/rtlog/snifferlog_latest.tgz ./build/images/

#build-slogsubscriber:
#	@echo "build slogsubscriber binary"
#	cd builder/slogsubscriber && rm -f slogsubscriber  && ./build.sh
#	mv builder/slogsubscriber/slogsubscriber ./build/bin/

build-snifferne:
	@echo "build netscan binary from sniffer_ne image"
	cd builder/sniffer_ne && rm -f netscan  && ./build.sh
	mv builder/sniffer_ne/netscan ./build/bin/

build-labo:
	@echo "build labo binary from labo image"
	cd builder/labo && rm -f labo  && ./build.sh
	mv builder/labo/labo ./build/bin/

