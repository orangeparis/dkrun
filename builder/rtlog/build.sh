
# build image
docker build --force-rm --no-cache --network host -t snifferlog .

# archive image
docker save snifferlog:latest | gzip > snifferlog_latest.tgz