
# build image
docker build --force-rm --no-cache --network host -t lbservices .

# archive image
docker save lbservices:latest | gzip > lbservices_latest.tgz