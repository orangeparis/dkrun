# Consul
echo Fetching Consul...
curl -sSL https://releases.hashicorp.com/consul/1.6.2/consul_1.6.2_linux_amd64.zip -o consul.zip
#wget --show-progress https://releases.hashicorp.com/consul/1.6.2/consul_1.6.2_linux_amd64.zip -o consul.zip
unzip consul.zip
rm -f consul.zip
chmod +x consul

# Nomad
echo Fetching Nomad...
curl -sSL https://releases.hashicorp.com/nomad/0.10.2/nomad_0.10.2_linux_amd64.zip -o nomad.zip
#wget --show-progress https://releases.hashicorp.com/nomad/0.10.2/nomad_0.10.2_linux_amd64.zip -o nomad.zip
unzip nomad.zip
rm -f nomad.zip
chmod +x nomad

# nats-server
echo Fetching nats...
curl -L https://github.com/nats-io/nats-server/releases/download/v2.1.2/nats-server-v2.1.2-linux-amd64.zip -o nats-server.zip
#wget --show-progress https://github.com/nats-io/nats-server/releases/download/v2.1.2/nats-server-v2.1.2-linux-amd64.zip -o nats-server.zip
unzip nats-server.zip
rm -f nats-server.zip
chmod +x nats-server

# resgate
echo Fetching resgate...
curl -L https://github.com/resgateio/resgate/releases/download/v1.4.1/resgate-v1.4.1-linux-amd64.zip -o resgate.zip
#wget --show-progress -o resgate.zip https://github.com/resgateio/resgate/releases/download/v1.4.1/resgate-v1.4.1-linux-amd64.zip
unzip resgate.zip
rm -f resgate.zip
chmod +x resgate
