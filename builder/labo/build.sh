# build image
docker build --force-rm --no-cache --network host -t labo .

# extract labo binary from image
echo "extract binary /go/bin/labo from image labo"
docker create -ti --name dummy labo:latest bash
docker cp dummy:/app/labo ./labo
docker rm -f dummy

docker rmi labo:latest